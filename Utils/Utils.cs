﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Windows;

namespace DataBaseDemo.Connect
{
    public class Utils
    {
        public DataTable GetExcel(string[] SheetNames, int Index, string FileName)
        {
            DataTable table = new DataTable();
            string constr = GetConStr(FileName);
            if (FileName != null)
            {
                string fileType = FileName.Substring(FileName.LastIndexOf("."));               
                if (fileType == ".csv")
                {
                    char c = ',';
                    switch (Index)
                    {
                        case 0: c = ',';break;
                        case 1: c = ';';break;
                        case 2: c = ' ';break;
                        case 3: c = '\t';break;
                    }
                    DataRow row;
                    string strline;
                    string[] line;
                    string[] str;
                    int t = 0;
                    var reader = new StreamReader(FileName);
                    if (reader.Peek() > 0)
                    {
                        line = reader.ReadLine().Split(c);                       
                        for (int i = 0; i < line.Length; i++)
                        {
                            DataColumn column = new DataColumn(line[i]);
                            table.Columns.Add(column);
                        }
                    }

                    while ((strline = reader.ReadLine()) != null)
                    {
                        line = strline.Split(c);
                        row = table.NewRow();
                        for (int i = 0; i < line.Length; i++)
                        {
                            row[i] = line[i];
                        }
                        table.Rows.Add(row);
                    }
                }
                if (fileType == ".xlsx")
                {
                    if (SheetNames == null) return table;
                    OleDbConnection conn = null;
                    DataTable dt = null;
                    try
                    {
                        
                        conn = new OleDbConnection(constr);
                        conn.Open();
                        string sql = $"Select * From [{SheetNames[Index]}$]";
                        OleDbDataAdapter ada = new OleDbDataAdapter(sql, constr);
                        DataSet ds = new DataSet();
                        ada.Fill(ds);
                        DataTableCollection dataTable = ds.Tables;
                        table = dataTable[0];
                        return table;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return null;
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                            conn.Dispose();
                        }
                        if (dt != null)
                        {
                            dt.Dispose();
                        }
                    }
                }
                if (fileType == ".db")
                {
                    if (SheetNames == null) return table;
                    SqliteConnection con = null;
                    try
                    {
                        con = new SqliteConnection(constr);
                        con.Open();

                        string sql = $"SELECT * FROM {SheetNames[Index]}";
                        using var cmd = new SqliteCommand(sql, con);
                        SqliteDataReader reader = cmd.ExecuteReader();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            DataColumn column = new DataColumn();
                            column.ColumnName = reader.GetName(i);
                            table.Columns.Add(column);
                        }
                        while (reader.Read())
                        {
                            DataRow rows = table.NewRow();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                rows[i] = reader.GetString(i);
                            }
                            table.Rows.Add(rows.ItemArray);
                        }
                        return table;
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        if ( con != null)
                        {
                            con.Close();
                            con.Dispose();
                        }
                    }             
                }              
            }
            return table;
        }

        public string[] GetSheetNames(string FileName)
        {
            string[] names = null;
            if (FileName != null)
            {
                string fileType = FileName.Substring(FileName.LastIndexOf("."));
                if (fileType == ".csv")
                {

                }
                if (fileType == ".xlsx")
                {
                    OleDbConnection conn = null;
                    DataTable dt = null;
                    try
                    {
                        string constr = GetConStr(FileName);
                        conn = new OleDbConnection(constr);
                        conn.Open();
                        dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);//获取工作表名的集合
                        if (dt == null) return null;
                        names = new string[dt.Rows.Count];
                        int i = 0;
                        foreach (DataRow row in dt.Rows)
                        {
                            string strSheetName = row["Table_Name"].ToString();
                            if (strSheetName.Contains("$") && strSheetName.Replace("'", "'").EndsWith("$"))
                            {
                                names[i] = strSheetName.Substring(0, strSheetName.Length - 1);
                            }
                            i++;
                        }
                        return names;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return null;
                    }
                }
                if (fileType == ".db")
                {
                    DataTable data = new DataTable();
                    string constr = GetConStr(FileName);
                    using var con = new SqliteConnection(constr);
                    con.Open();
                    string sql = "SELECT name FROM sqlite_master WHERE type = 'table' ORDER BY name";
                    using var cmd = new SqliteCommand(sql, con);
                    SqliteDataReader reader = cmd.ExecuteReader();
                    names = new string[reader.FieldCount];
                    while (reader.Read())
                    {
                        for(int i = 0; i < reader.FieldCount; i++)
                        {
                            names[i] = reader.GetString(0);
                        }
                    }
                    return names;
                }
            }
            return names;
        }

        private string GetConStr(string FileName)
        {
            string constr = string.Empty;
            if (FileName != null)
            {
                string fileType = FileName.Substring(FileName.LastIndexOf("."));
                if (fileType == ".csv")
                {
                    constr = $"Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + FileName.Remove(FileName.LastIndexOf("\\") + 1) + ";" + ";Extended Properties='Text;'";
                }
                if(fileType == ".xlsx")
                {
                    constr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + FileName + ";" + ";Extended Properties=\"Excel 8.0;\"";
                }
                if(fileType == ".db")
                {
                    constr = $"DataSource = {FileName}";
                }
            }
            else
            {
                return null;
            }
            return constr;
        }  
        
        public DataTable MinSort(DataTable dt,int i)
        {
            foreach (DataRow row in dt.Rows)
            {
                for (int j = 0; j < dt.Rows.Count - 1; j++)
                {
                    if (int.TryParse(dt.Rows[j].ItemArray[i].ToString(), out int i1) && int.TryParse(dt.Rows[j + 1].ItemArray[i].ToString(), out int i2))
                    {
                        if (int.Parse(dt.Rows[j].ItemArray[i].ToString()) < int.Parse(dt.Rows[j + 1].ItemArray[i].ToString()))
                        {
                            var t = dt.Rows[j].ItemArray[i];
                            dt.Rows[j].SetField(i, dt.Rows[j + 1].ItemArray[i]);
                            dt.Rows[j + 1].SetField(i, t);
                        }
                    }
                    else
                    {
                        if (string.Compare(dt.Rows[j].ItemArray[i].ToString(), dt.Rows[j + 1].ItemArray[i].ToString()) < 0)
                        {
                            var t = dt.Rows[j].ItemArray[i];
                            dt.Rows[j].SetField(i, dt.Rows[j + 1].ItemArray[i]);
                            dt.Rows[j + 1].SetField(i, t);
                        }
                    }
                }
            }
            return dt;
        }

        public DataTable MaxSort(DataTable dt,int i)
        {
            foreach (DataRow row in dt.Rows)
            {
                for (int j = 0; j < dt.Rows.Count - 1; j++)
                {
                    if (int.TryParse(dt.Rows[j].ItemArray[i].ToString(), out int i1) && int.TryParse(dt.Rows[j + 1].ItemArray[i].ToString(), out int i2))
                    {
                        if (int.Parse(dt.Rows[j].ItemArray[i].ToString()) > int.Parse(dt.Rows[j + 1].ItemArray[i].ToString()))
                        {
                            var t = dt.Rows[j].ItemArray[i];
                            dt.Rows[j].SetField(i, dt.Rows[j + 1].ItemArray[i]);
                            dt.Rows[j + 1].SetField(i, t);
                        }
                    }
                    else
                    {
                        if (string.Compare(dt.Rows[j].ItemArray[i].ToString(), dt.Rows[j + 1].ItemArray[i].ToString()) > 0)
                        {
                            var t = dt.Rows[j].ItemArray[i];
                            dt.Rows[j].SetField(i, dt.Rows[j + 1].ItemArray[i]);
                            dt.Rows[j + 1].SetField(i, t);
                        }
                    }
                }
            }
            return dt;
        }

        public DataTable RemoveHead(DataTable Table)
        {
            DataTable table = new DataTable();
            if (Table != null)
            {
                DataRow row = table.NewRow();
                for (int i = 0; i < Table.Columns.Count; i++)
                {
                    DataColumn column = new DataColumn();
                    column.ColumnName = "F" + (i + 1);
                    table.Columns.Add(column);
                    row[i] = Table.Columns[i].ColumnName;
                }
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow rows = table.NewRow();
                    rows = Table.Rows[i];
                    table.Rows.Add(rows.ItemArray);
                }
                table.Rows.InsertAt(row, 0);
            }
            return table;
        }
        public DataTable AddHeade(DataTable Table)
        {
            int k = 0;
            int t = 0;
            DataTable table = new DataTable();
            if (Table != null)
            {               
                var row = Table.Rows[0];
                foreach (DataColumn col in Table.Columns)
                {
                    t++;
                    if (col.ColumnName.Equals("F" + t))
                    {
                        k++;
                    }
                }
                if (k == Table.Columns.Count)
                {
                    for (int i = 0; i < Table.Columns.Count; i++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = row[i].ToString();
                        table.Columns.Add(column);
                    }
                    for (int i = 0; i < Table.Rows.Count; i++)
                    {
                        DataRow rows = table.NewRow();
                        rows = Table.Rows[i];
                        table.Rows.Add(rows.ItemArray);
                    }
                    table.Rows.Remove(table.Rows[0]);                   
                }
            }
            return table;
        }
    }
}
