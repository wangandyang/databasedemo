﻿
using DataBaseDemo.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reactive.Disposables;
using System.Text;

namespace DataBaseDemo.ViewModels
{
    public class FilterViewModel : BindableBase, IDialogAware,IDisposable
    {
        private readonly CompositeDisposable _disposables = new CompositeDisposable();
        public FilterViewModel()
        {
            Table = new ReactiveProperty<DataTable>().AddTo(_disposables);
            Text = new ReactiveProperty<string>().AddTo(_disposables);
            CIndex = new ReactiveProperty<int>(0).AddTo(_disposables);
            FIndex = new ReactiveProperty<int>(0).AddTo(_disposables);
            ColumnName = new ReactiveProperty<string[]>().AddTo(_disposables);
            Conditions = new ReactiveProperty<string[]>(DefaultCondition()).AddTo(_disposables);
            IsEnable = new ReactiveProperty<bool>().AddTo(_disposables);
            Text.Subscribe(x => { Enable(); }).AddTo(_disposables);
        }

        private void Enable()
        {
            if(Text.Value != null )
            {
                if(Text.Value.Trim() != "")
                {
                    IsEnable.Value = true;
                }
                else
                {
                    IsEnable.Value = false;
                }
            }
            else
            {
                IsEnable.Value = false;
            }
        }

        public ReactiveProperty<DataTable> Table { get; }
        public ReactiveProperty<string> Text { get; }
        public ReactiveProperty<int> CIndex { get; }
        public ReactiveProperty<int> FIndex { get; }
        public ReactiveProperty<string[]> ColumnName { get; }
        public ReactiveProperty<string[]> Conditions { get; }
        public ReactiveProperty<bool> IsEnable { get; }

        private DelegateCommand submit;
        public DelegateCommand Submit
        {
            get { if (submit == null) submit = new DelegateCommand(Submited);return submit; }
        }

        private void Submited()
        {
            DataTable dt = Table.Value.Copy();
            dt.Clear();
            foreach(DataRow row in Table.Value.Rows)
            {
                var rows = row[0].ToString();
                switch (CIndex.Value)
                {
                    case 0: 
                        if (row[FIndex.Value].ToString().Equals(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    case 1:
                        if (!row[FIndex.Value].ToString().Equals(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    case 2:
                        if (row[FIndex.Value].ToString().StartsWith(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    case 3:
                        if (!row[FIndex.Value].ToString().StartsWith(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    case 4:
                        if (row[FIndex.Value].ToString().EndsWith(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    case 5:
                        if (!row[FIndex.Value].ToString().EndsWith(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    case 6:
                        if (row[FIndex.Value].ToString().Contains(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    case 7:
                        if (!row[FIndex.Value].ToString().Contains(Text.Value))
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                        break;
                    default: break;
                }                
            }
            Table.Value = dt;
            OnDialogClosed();
        }

        private string[] DefaultName()
        {
            var count = 0;
            if(Table.Value != null)
            {
                count = Table.Value.Columns.Count;
            }
            string[] columnName = new string[count];
            for(int i = 0; i < count; i++)
            {
                columnName[i] = Table.Value.Columns[i].ColumnName;
            }
            return columnName;
        }

        private string[] DefaultCondition()
        {
            string[] condition = {"equals","does not equals","begins with","does not begins with","ends with","does not ends with","contains","does not contains"};
            return condition;
        }

        public string Title => "Filter";

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            DialogParameters keys = new DialogParameters();
            keys.Add("Table", Table.Value);
            RequestClose?.Invoke(new DialogResult(ButtonResult.OK, keys));
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            Table.Value = parameters.GetValue<DataTable>("Tables");
            ColumnName.Value = DefaultName();
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }

    }
}
