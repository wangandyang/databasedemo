﻿using DataBaseDemo.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseDemo.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private readonly IRegionManager regionManager;
        public MainViewModel(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
            regionManager.RegisterViewWithRegion("ContentRegion", typeof(CanvasView));
        }
        private DelegateCommand open;
        public DelegateCommand Open
        {
            get
            {
                if(open == null)
                {
                    open = new DelegateCommand(SelectFile);
                }
                return open;
            }
        }

        private void SelectFile()
        {
            regionManager.Regions["DataRegion"].RequestNavigate("DataTableView", callback =>
            {

            });
        }
    }
}
