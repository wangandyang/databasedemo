﻿using DataBaseDemo.Connect;
using DataBaseDemo.Model;
using DataBaseDemo.Views;
using GalaSoft.MvvmLight.Messaging;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reactive.Disposables;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DataBaseDemo.ViewModels
{
    public class DataTableViewModel : BindableBase, IDisposable, INavigationAware
    {
        private readonly IRegionManager regionManager;
        private readonly CompositeDisposable _disposables = new CompositeDisposable();
        IDialogService dialog;
        private bool isChecked;
        public DataTableViewModel(IDialogService dialog, IRegionManager regionManager)
        {
            this.dialog = dialog;
            this.regionManager = regionManager;
            Index = new ReactiveProperty<int>(-1).AddTo(_disposables);
            Index.Subscribe(x => { if (x > -1) Get(); }).AddTo(_disposables);
            Sheet = new ReactiveProperty<string[]>().AddTo(_disposables);
            Table = new ReactiveProperty<DataTable>().AddTo(_disposables);
            Tables = new ReactiveProperty<DataTable>().AddTo(_disposables);
            Num = new ReactiveProperty<string>().AddTo(_disposables);
            TableIndex = new ReactiveProperty<int>(-1).AddTo(_disposables);
            TableIndex.Subscribe(x => { IndexChange(); }).AddTo(_disposables);
            Column = new ReactiveProperty<int>(-1).AddTo(_disposables);
            FileName = new ReactiveProperty<string>().AddTo(_disposables);
            File = new ReactiveProperty<string>().AddTo(_disposables);
            Visable = new ReactiveProperty<Visibility>(Visibility.Hidden).AddTo(_disposables);
            IsEnable = new ReactiveProperty<bool>(true).AddTo(_disposables);
            IsEnabled = new ReactiveProperty<bool>(false).AddTo(_disposables);
            Item = new ReactiveProperty<object>().AddTo(_disposables);
            Item.Subscribe(x => { }).AddTo(_disposables);
            HeaderIndex = new ReactiveProperty<int>(-1).AddTo(_disposables);
            CV = new ReactiveProperty<Visibility>(Visibility.Visible).AddTo(_disposables);
        }

        public ReactiveProperty<string[]> Sheet { get; }
        public ReactiveProperty<DataTable> Table { get; }
        public ReactiveProperty<DataTable> Tables { get; }
        public ReactiveProperty<int> Index { get; }
        public ReactiveProperty<string> FileName { get; }
        public ReactiveProperty<string> File { get; }
        public ReactiveProperty<bool> IsEnable { get; }
        public ReactiveProperty<bool> IsEnabled { get; }
        public ReactiveProperty<Visibility> Visable { get; }
        public ReactiveProperty<int> TableIndex { get; }
        public ReactiveProperty<Visibility> CV { get; }
        public ReactiveProperty<int> Column { get; }
        public ReactiveProperty<object> Item { get; }
        public ReactiveProperty<int> HeaderIndex { get; set; }
        public ReactiveProperty<string> Num { get; }

        private DelegateCommand close;
        public DelegateCommand Close
        {
            get { if (close == null) close = new DelegateCommand(Closed); return close; }
        }

        private DelegateCommand search;
        public DelegateCommand Search
        {
            get { if (search == null) search = new DelegateCommand(Searched); return search; }
        }

        private DelegateCommand filter;
        public DelegateCommand Filter
        {
            get { if (filter == null) filter = new DelegateCommand(Filtered); return filter; }
        }

        private DelegateCommand maxSort;
        public DelegateCommand MaxSort
        {
            get { if (maxSort == null) maxSort = new DelegateCommand(MaxSorted); return maxSort; }
        }

        private void MaxSorted()
        {
            DataTable dt = new DataTable();
            dt = Table.Value.Copy();
            var sort = new Utils();
            var i = HeaderIndex.Value;
            if (i > -1)
            {
                dt = sort.MaxSort(dt, i);
                //view.Sort = $"{Table.Value.Columns[i].ColumnName} ASC";
            }
            else
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    sort.MaxSort(dt, j);
                }
            }
            Table.Value = dt;
        }

        private DelegateCommand minSort;
        public DelegateCommand MinSort
        {
            get { if (minSort == null) minSort = new DelegateCommand(MinSorted); return minSort; }
        }

        private void MinSorted()
        {
            DataTable dt = new DataTable();
            dt = Table.Value.Copy();
            var sort = new Utils();
            var i = HeaderIndex.Value;
            if (i > -1)
            {

                dt = sort.MinSort(dt, i);
                //view.Sort = @$"{$(Table.Value.Columns[i].ColumnName)} DESC";
            }
            else
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    sort.MinSort(dt, j);
                }
            }
            Table.Value = dt;
        }

        private DelegateCommand cancel;
        public DelegateCommand Cancel
        {
            get { if (cancel == null) cancel = new DelegateCommand(Canceled); return cancel; }
        }

        private DelegateCommand<MouseEventArgs> mouseMoveCommand;
        public DelegateCommand<MouseEventArgs> MouseMoveCommand
        {
            get { if (mouseMoveCommand == null) mouseMoveCommand = new DelegateCommand<MouseEventArgs>(MouseMoveCommanded); return mouseMoveCommand; }
        }

        private DelegateCommand<SelectionChangedEventArgs> selectionCommand;
        public DelegateCommand<SelectionChangedEventArgs> SelectionCommand
        {
            get { if (selectionCommand == null) selectionCommand = new DelegateCommand<SelectionChangedEventArgs>(SelectionCommanded); return selectionCommand; }
        }

        private void SelectionCommanded(SelectionChangedEventArgs e)
        {
            var dt = e.OriginalSource as DataGrid;
            if (Column.Value != -1)
            {
                dt.Focus();
                dt.ScrollIntoView(Item.Value, dt.Columns[Column.Value]);
            }
        }

        private void MouseMoveCommanded(MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var header = e.OriginalSource as ContentControl;
                if (header != null)
                {
                    var i = 0;
                    if (TableIndex.Value > -1) i = TableIndex.Value;
                    HeaderIndex.Value = header.TabIndex;
                    DragDrop.DoDragDrop(header, header.TabIndex, DragDropEffects.Move);
                    e.Handled = true;
                    if (HeaderIndex != null) Messenger.Default.Send<int>(HeaderIndex.Value, "HeaderIndex");
                }
            }
            Messenger.Default.Send<DataTable>(Table.Value, "Table");
            Messenger.Default.Send<int>(TableIndex.Value, "TableIndex");
        }

        private void IndexChange()
        {
            Messenger.Default.Send<DataTable>(Table.Value, "Table");
            Messenger.Default.Send<int>(TableIndex.Value, "TableIndex");
            if (Num != null)
                Num.Value = (TableIndex.Value + 1).ToString() + "/" + Table.Value?.Rows.Count.ToString();
        }

        private void Canceled()
        {
            Table.Value = Tables.Value;
            IsEnable.Value = true;
            IsEnabled.Value = false;
        }

        private void Filtered()
        {
            DialogParameters parameters = new DialogParameters();
            parameters.Add("Tables", Table.Value);
            dialog.ShowDialog("FilterView", parameters, callback =>
            {
                if (callback.Result == ButtonResult.OK)
                {
                    Tables.Value = Table.Value;
                    Table.Value = callback.Parameters.GetValue<DataTable>("Table");
                    if (Table.Value.Rows.Count == 0)
                    {
                        MessageBox.Show("筛选结果无数据！");
                    }
                    IsEnable.Value = false;
                    IsEnabled.Value = true;
                }
            });
        }

        private void Searched()
        {
            DialogParameters parameters = new DialogParameters();
            parameters.Add("Table", Table);
            parameters.Add("TableIndex", TableIndex);
            parameters.Add("Item", Item);
            parameters.Add("Column", Column);
            dialog.ShowDialog("SearchView", parameters, callBack =>
             {

             });
        }

        private void Closed()
        {
            Visable.Value = Visibility.Hidden;
        }

        private void Get()
        {
            if (Sheet != null && Index != null && FileName != null)
            {
                var con = new Utils();
                if (isChecked == true)
                {
                    Table.Value = con.GetExcel(Sheet.Value, Index.Value, FileName.Value);
                }
                else
                {
                    Table.Value = con.GetExcel(Sheet.Value, Index.Value, FileName.Value);
                    Table.Value = con.RemoveHead(Table.Value);
                }
                if (Num != null)
                    Num.Value = "1" + "/" + Table.Value?.Rows.Count.ToString();
            }
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            dialog.ShowDialog("FileView", callback =>
            {
                if (callback.Result == ButtonResult.OK)
                {
                    isChecked = callback.Parameters.GetValue<bool>("IsChecked");
                    Sheet.Value = callback.Parameters.GetValue<string[]>("SheetNames");
                    Table.Value = callback.Parameters.GetValue<DataTable>("Table");
                    FileName.Value = callback.Parameters.GetValue<string>("FileName");
                    Index.Value = callback.Parameters.GetValue<int>("Index");
                    Visable.Value = Visibility.Visible;
                    CV.Value = callback.Parameters.GetValue<Visibility>("CV");
                    Num.Value = "1" + "/" + Table.Value.Rows.Count.ToString();
                }
                else
                {
                    Visable.Value = Visibility.Hidden;
                }
            });
            if (FileName.Value != null) File.Value = FileName.Value.Substring(FileName.Value.LastIndexOf("\\") + 1);
        }
    }
}
