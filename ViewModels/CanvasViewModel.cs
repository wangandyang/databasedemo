﻿using DataBaseDemo.Views;
using GalaSoft.MvvmLight.Messaging;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reactive.Disposables;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace DataBaseDemo.ViewModels
{
    public class CanvasViewModel : BindableBase, IDisposable
    {
        private readonly CompositeDisposable _disposables = new CompositeDisposable();
        private int i = 0;  // 拖动生成的TextBox列表索引
        private bool isCanMove = false;  // 鼠标是否移动
        private Point tempStartPoint;  // 起始坐标
        public CanvasViewModel()
        {
            Table = new ReactiveProperty<DataTable>().AddTo(_disposables);
            TableIndex = new ReactiveProperty<int>(-1).AddTo(_disposables);
            TableIndex.Subscribe(x => { TextChanged(); }).AddTo(_disposables);
            Text = new ReactiveProperty<string>().AddTo(_disposables);
            HeaderIndex = new ReactiveProperty<int>().AddTo(_disposables);
            Index = new ReactiveProperty<int[]>().AddTo(_disposables);
            TBox = new ReactiveProperty<TextBox[]>().AddTo(_disposables);
            Bor = new ReactiveProperty<Border>().AddTo(_disposables);
            Canvass = new ReactiveProperty<Canvas>().AddTo(_disposables);
            IsEnable = new ReactiveProperty<bool>(false).AddTo(_disposables);
            TBox.Value = new TextBox[100];
            Index.Value = new int[100];
            Messenger.Default.Register<DataTable>(this, "Table", Tabledata);
            Messenger.Default.Register<int>(this, "TableIndex", TableIndexChanged);
            Messenger.Default.Register<int>(this, "HeaderIndex", HeaderIndexChanged);
        }

        public ReactiveProperty<DataTable> Table { get; }
        public ReactiveProperty<int> TableIndex { get; set; }
        public ReactiveProperty<int> HeaderIndex { get; set; }
        public ReactiveProperty<int[]> Index { get; }
        public ReactiveProperty<string> Text { get; set; }
        public ReactiveProperty<TextBox[]> TBox { get; }
        public ReactiveProperty<Border> Bor { get; }
        public ReactiveProperty<Canvas> Canvass { get; }
        public ReactiveProperty<bool> IsEnable { get; }

        private DelegateCommand<DragEventArgs> dropCommand;
        public DelegateCommand<DragEventArgs> DropCommand
        {
            get { if (dropCommand == null) dropCommand = new DelegateCommand<DragEventArgs>(Droped); return dropCommand; }
        }
        private void Droped(DragEventArgs e)
        {
            if (TableIndex.Value <= -1) TableIndex.Value = 0;
            TBox.Value[i] = new TextBox();
            var canvas = e.OriginalSource as Canvas;
            var pos = e.GetPosition(canvas);
            TBox.Value[i].Margin = new Thickness(pos.X, pos.Y, pos.X, pos.Y);
            if(e.Data.GetData(typeof(int))!=null)
            Index.Value[i] = (int)e.Data.GetData(typeof(int));
            TBox.Value[i].BorderBrush = new SolidColorBrush(Colors.Gray);
            TBox.Value[i].Text = Table.Value?.Rows[TableIndex.Value].ItemArray[Index.Value[i]].ToString();
            canvas.Children.Add(TBox.Value[i]);
            //canvas.Children.Remove((UIElement)canvas.FindName("Text"+i));
            i++;
        }

        private DelegateCommand<MouseEventArgs> mouseMoveCommand;
        public DelegateCommand<MouseEventArgs> MouseMoveCommand
        {
            get { if (mouseMoveCommand == null) mouseMoveCommand = new DelegateCommand<MouseEventArgs>(MouseMoved); return mouseMoveCommand; }
        }

        /// <summary>
        /// 框选
        /// </summary>
        /// <param name="e"></param>
        private void MouseMoved(MouseEventArgs e)
        {
            if (isCanMove)
            {
                Point tempEndPoint = e.GetPosition(Canvass.Value);
                DrawMultiselectBorder(tempEndPoint, tempStartPoint, Canvass.Value);
            }
        }

        private void DrawMultiselectBorder(Point endPoint, Point startPoint, Canvas canvas)
        {
            if (Bor.Value == null)
            {
                Bor.Value = new Border();
                Bor.Value.Background = new SolidColorBrush(Colors.Gray);
                Bor.Value.Opacity = 0.4;
                Bor.Value.BorderThickness = new Thickness(2);
                Bor.Value.BorderBrush = new SolidColorBrush(Colors.DarkGray);
                Canvas.SetZIndex(Bor.Value, 100);              
                canvas?.Children?.Add(Bor.Value);
            }
            Bor.Value.Width = Math.Abs(endPoint.X - startPoint.X);
            Bor.Value.Height = Math.Abs(endPoint.Y - startPoint.Y);
            if (endPoint.X - startPoint.X >= 0)
            {
                Canvas.SetLeft(Bor.Value, startPoint.X);
            }
            else
            {
                Canvas.SetLeft(Bor.Value, endPoint.X);
            }
            if (endPoint.Y - startPoint.Y >= 0)
            {
                Canvas.SetTop(Bor.Value, startPoint.Y);
            }
            else
            {
                Canvas.SetTop(Bor.Value, endPoint.Y);
            }
        }

        private DelegateCommand<MouseButtonEventArgs> mouseLeftButtonUpCommand;
        public DelegateCommand<MouseButtonEventArgs> MouseLeftButtonUpCommand
        {
            get { if (mouseLeftButtonUpCommand == null) mouseLeftButtonUpCommand = new DelegateCommand<MouseButtonEventArgs>(MouseLeftButtonUped); return mouseLeftButtonUpCommand; }
        }

        private void MouseLeftButtonUped(MouseButtonEventArgs e)
        {
            if(Bor.Value != null)
            {
                //获取选框的矩形位置
                Point tempEndPoint = e.GetPosition(Canvass.Value);
                Rect tempRect = new Rect(tempStartPoint, tempEndPoint);    
                // 获取子控件
                List<TextBox> childList = GetChildObject<TextBox>(Canvass.Value);
                if (childList.Count > 0) IsEnable.Value = true;
                foreach (var child in childList)
                {
                    Rect childRect = new Rect(child.Margin.Left, child.Margin.Top, child.ActualWidth, child.ActualHeight);
                    if (childRect.IntersectsWith(tempRect))
                    {
                        child.BorderBrush = new SolidColorBrush(Colors.Blue);
                        child.BorderThickness = new Thickness(2);
                    }
                }
                Canvass.Value?.Children.Remove(Bor.Value);
                Bor.Value = null;
            }
            isCanMove = false;
        }

        private static List<T> GetChildObject<T>(DependencyObject obj) where T:FrameworkElement
        {
            DependencyObject child = null;
            List<T> childList = new List<T>();
            if(obj!=null)
            for(int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                child = VisualTreeHelper.GetChild(obj, i);
                if(child is T)
                {
                    childList.Add((T)child);
                }
                childList.AddRange(GetChildObject<T>(child));
            }
            return childList;
        }

        private DelegateCommand<MouseButtonEventArgs> mouseLeftButtonDownCommand;
        public DelegateCommand<MouseButtonEventArgs> MouseLeftButtonDownCommand
        {
            get { if (mouseLeftButtonDownCommand == null) mouseLeftButtonDownCommand = new DelegateCommand<MouseButtonEventArgs>(MouseLeftButtonDowned); return mouseLeftButtonDownCommand; }
        }

        /// <summary>
        /// 鼠标按下记录起始点
        /// </summary>
        /// <param name="e"></param>
        private void MouseLeftButtonDowned(MouseButtonEventArgs e)
        {
            isCanMove = true;
            Canvass.Value = e.OriginalSource as Canvas;
            tempStartPoint = e.GetPosition(Canvass.Value);
        }

        private DelegateCommand deleteCommand;
        public DelegateCommand DeleteCommand
        {
            get { if (deleteCommand == null) deleteCommand = new DelegateCommand(Deleted);return deleteCommand; }
        }

        private void Deleted()
        {
            List<TextBox> childList = GetChildObject<TextBox>(Canvass.Value);
            foreach (var child in childList)
            {            
                if (child.BorderThickness == new Thickness(2))
                {
                    Canvass.Value.Children.Remove(child);
                }
            }
            IsEnable.Value = false;
        }

        private DelegateCommand cancelCommand;
        public DelegateCommand CanCelCommand
        {
            get { if (cancelCommand == null) cancelCommand = new DelegateCommand(Canceled);return cancelCommand; }
        }

        private void Canceled()
        {
            List<TextBox> childList = GetChildObject<TextBox>(Canvass.Value);
            foreach (var child in childList)
            {
                if (child.BorderThickness == new Thickness(2))
                {
                    child.BorderThickness = new Thickness(1);
                    child.BorderBrush = new SolidColorBrush(Colors.Gray);
                }
            }
            IsEnable.Value = false;
        }

        private void Tabledata(DataTable obj)
        {
            Table.Value = obj;
        }

        private void TableIndexChanged(int obj)
        {
            TableIndex.Value = obj;
        }

        private void HeaderIndexChanged(int obj)
        {
            HeaderIndex.Value = obj;
        }

        private void TextChanged()
        {
            if (TBox != null && TableIndex.Value > -1 && TableIndex.Value < Table.Value?.Rows.Count)
            {
                for (int j = 0; j < i; j++)
                {
                    var x = TBox.Value[j];
                    TBox.Value[j].Text = Table.Value?.Rows[TableIndex.Value].ItemArray[Index.Value[j]].ToString();
                }
            }
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
