﻿using DataBaseDemo.Connect;
using DataBaseDemo.Views;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Reactive.Disposables;
using System.Text;
using System.Windows;

namespace DataBaseDemo.ViewModels
{
    public class FileViewModel : BindableBase, IDisposable, IDialogAware
    {
        private readonly CompositeDisposable _disposables = new CompositeDisposable();
        public FileViewModel()
        {
            FileName = new ReactiveProperty<string>().AddTo(_disposables);
            IsChecked = new ReactiveProperty<bool>(true).AddTo(_disposables);
            IsChecked.Subscribe(x => { if (x == true) AddHeade(); if (x == false) RemoveHead(); }).AddTo(_disposables);
            SheetNames = new ReactiveProperty<string[]>().AddTo(_disposables);
            TableList = new ReactiveProperty<DataTable>().AddTo(_disposables);
            IsEnable = new ReactiveProperty<bool>(false).AddTo(_disposables);
            Index = new ReactiveProperty<int>(-1).AddTo(_disposables);
            Index.Subscribe(x => { if (x > -1) Get(); }).AddTo(_disposables);
            Delimit = new ReactiveProperty<string[]>(DefaultValue()).AddTo(_disposables);
            IsVisibility = new ReactiveProperty<Visibility>(Visibility.Hidden).AddTo(_disposables);
            DeIndex = new ReactiveProperty<int>(0).AddTo(_disposables);
            DeIndex.Subscribe(x => { GetData(); }).AddTo(_disposables);
            CV = new ReactiveProperty<Visibility>(Visibility.Visible).AddTo(_disposables);
            Time = new ReactiveProperty<string>().AddTo(_disposables);
        }

        public ReactiveProperty<string> FileName { get; }
        public ReactiveProperty<bool> IsChecked { get; }
        public ReactiveProperty<Visibility> CV { get; }
        public ReactiveProperty<int> Index { get; }
        public ReactiveProperty<int> DeIndex { get; }
        public ReactiveProperty<string[]> SheetNames { get; }
        public ReactiveProperty<DataTable> TableList { get; }
        public ReactiveProperty<bool> IsEnable { get; }
        public ReactiveProperty<string[]> Delimit { get; }
        public ReactiveProperty<Visibility> IsVisibility { get; }
        public ReactiveProperty<string> Time { get; }

        private DelegateCommand select;
        public DelegateCommand Select
        {
            get
            {
                if (select == null)
                {
                    select = new DelegateCommand(SelectedFile);
                }
                return select;
            }
        }

        // 打开选择文件窗口，选择文件，并获取数据
        private void SelectedFile()
        {
            var dialog = new OpenFileDialog();
            var con = new Utils();
            dialog.Filter = "DataBase文件|*.xlsx;*.xls;*.csv;*.db";
            if (dialog.ShowDialog() == false) return;
            FileName.Value = dialog.FileName;
            Index.Value = 0;
            IsEnable.Value = true;
            string fileType = FileName.Value.Substring(FileName.Value.LastIndexOf("."));
            if (FileName.Value != null)
            {
                if (fileType == ".csv")
                {
                    IsVisibility.Value = Visibility.Visible;
                    Index.Value = DeIndex.Value;
                    CV.Value = Visibility.Hidden;
                }
                else
                {
                    CV.Value = Visibility.Visible;
                    IsVisibility.Value = Visibility.Hidden;
                }
            }
            IsChecked.Value = true;
            Stopwatch watch = new Stopwatch();
            watch.Start();
            SheetNames.Value = con.GetSheetNames(FileName.Value);
            TableList.Value = con.GetExcel(SheetNames.Value, Index.Value, FileName.Value);
            watch.Stop();
            TimeSpan time = watch.Elapsed;
            Time.Value = "读取时间：" + time.TotalMilliseconds.ToString() + "ms";
            DeIndex.Value = 0;
        }

        private DelegateCommand submit;

        public DelegateCommand Submit
        {
            get
            {
                if (submit == null)
                {
                    submit = new DelegateCommand(SendData);
                }
                return submit;
            }
        }

        private void SendData()
        {
            OnDialogClosed();
        }

        private void AddHeade()
        {
            if (TableList != null)
            {
                var utl = new Utils();
                TableList.Value = utl.AddHeade(TableList.Value);
            }
        }
        public void RemoveHead()
        {
            if (TableList != null)
            {
                var utl = new Utils();
                TableList.Value = utl.RemoveHead(TableList.Value);
            }
        }

        private void Get()
        {
            var con = new Utils();
            Stopwatch watch = new Stopwatch();
            watch.Start();
            TableList.Value = con.GetExcel(SheetNames.Value, Index.Value, FileName.Value);
            watch.Stop();
            TimeSpan time = watch.Elapsed;
            Time.Value = "读取时间：" + time.TotalMilliseconds.ToString() + "ms";
            if (IsChecked.Value == false) RemoveHead();
        }

        private void GetData()
        {
            var con = new Utils();
            if (IsChecked.Value == true)
            {
                TableList.Value = con.GetExcel(SheetNames.Value, DeIndex.Value, FileName.Value);
            }
            else
            {
                TableList.Value = con.GetExcel(SheetNames.Value, DeIndex.Value, FileName.Value);
                RemoveHead();
            }
        }

        private string[] DefaultValue()
        {
            string[] str = { "Comma", "Semicolon", "Space", "Tab" };
            return str;
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }

        public string Title => "";
        public event Action<IDialogResult> RequestClose;
        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            DialogParameters keys = new DialogParameters();
            keys.Add("SheetNames", SheetNames.Value);
            keys.Add("Table", TableList.Value);
            string fileType = FileName?.Value?.Substring(FileName.Value.LastIndexOf("."));
            if (fileType == ".csv")
            {
                Index.Value = DeIndex.Value;
            }
            keys.Add("Index", Index.Value);
            keys.Add("FileName", FileName.Value);
            keys.Add("CV", CV.Value);
            keys.Add("IsChecked", IsChecked.Value);
            RequestClose?.Invoke(new DialogResult(ButtonResult.OK, keys));
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {

        }
    }
}
