﻿using DataBaseDemo.Model;
using DataBaseDemo.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reactive.Disposables;
using System.Text;
using System.Windows;

namespace DataBaseDemo.ViewModels
{
    public class SearchViewModel : BindableBase, IDialogAware, IDisposable
    {
        private readonly CompositeDisposable _disposables = new CompositeDisposable();
        public SearchViewModel()
        {
            Table = new ReactiveProperty<DataTable>().AddTo(_disposables);
            Text = new ReactiveProperty<string>().AddTo(_disposables);
            TableIndex = new ReactiveProperty<int>(-1).AddTo(_disposables);
            Column = new ReactiveProperty<int>().AddTo(_disposables);
            Item = new ReactiveProperty<object>().AddTo(_disposables);
        }
        public ReactiveProperty<DataTable> Table { get; set; }
        public ReactiveProperty<int> TableIndex { get; set; }
        public ReactiveProperty<int> Column { get; set; }
        public ReactiveProperty<object> Item { get; set; }
        public ReactiveProperty<string> Text { get; }

        private DelegateCommand first;
        public DelegateCommand First
        {
            get{ if(first == null) first = new DelegateCommand(Search); return first; }
        }

        private DelegateCommand next;
        public DelegateCommand Next
        {
            get { if (next == null) next = new DelegateCommand(Nexted);return next; }
        }

        private DelegateCommand close;
        public DelegateCommand Close
        {
            get { if (close == null) close = new DelegateCommand(Closed);return close; }
        }

        private void Closed()
        {
            RequestClose?.Invoke(new DialogResult(ButtonResult.OK));
        }

        private void Search()
        {
            i = 0;
            var list = GetList();
            if (list.Count > 0)
            {
                Column.Value = list[i].Colum;
                TableIndex.Value = list[i].Row;
                //Item.Value = Table.Value.Rows[list[i].Colum].ItemArray[list[i].Row];
            }
            else
            {
                MessageBox.Show("未输入关键词！");
            }
            i++;
        }

        private int i = 0;
        private void Nexted()
        {
            var list = GetList();
            if (list.Count > 0)
            {
                if (i < list.Count)
                {
                    Column.Value = list[i].Colum;
                    TableIndex.Value = list[i].Row;
                }
                else
                {
                    i = 0;
                    Column.Value = list[i].Colum;
                    TableIndex.Value = list[i].Row;
                }
            }
            else
            {
                MessageBox.Show("未输入关键词！");
            }
            i++;
        }

        private List<Tables> GetList()
        {
            var list = new List<Tables>();
            var view = new DataTableView();
            Tables x = null;
            DataTable table = Table.Value;
            int j = 0;
            foreach (DataRow rows in table.Rows)
            {               
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (Text.Value!= null && rows[i].ToString().Contains(Text.Value))
                    {
                        x = new Tables();
                        x.Colum = i;
                        x.Row = j;
                        list.Add(x);
                    }
                }
                j++;
            }
            return list;
        }

        public string Title => "Find";

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {

        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            Table = parameters.GetValue<ReactiveProperty<DataTable>>("Table");
            TableIndex = parameters.GetValue<ReactiveProperty<int>>("TableIndex");
            Column = parameters.GetValue<ReactiveProperty<int>>("Column");
            Item = parameters.GetValue<ReactiveProperty<object>>("Item");
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
