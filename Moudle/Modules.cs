﻿using DataBaseDemo.ViewModels;
using DataBaseDemo.Views;
using Prism.Ioc;
using Prism.Modularity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataBaseDemo.Moudle
{
    public class Modules : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<DataTableView, DataTableViewModel>();
        }
    }
}
